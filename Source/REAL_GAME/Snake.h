// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Snake.generated.h"

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

class ASnakeElem;

UCLASS()
class REAL_GAME_API ASnake : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnake();
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElem> BodyClass;

	UPROPERTY()
	TArray<ASnakeElem*> SnakeBody;

	UPROPERTY(EditDefaultsOnly)
	float ElemPadding;

	UPROPERTY()
	EMovementDirection LastDirection;

	UPROPERTY()
	int32 ElemsQueue;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	float speed;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void AddElements(int ElemNum = 1);

	void Move();

	void FirstElemAdd(int ElemNum = 1);

	UFUNCTION()
	void OnElemOverlap(ASnakeElem* OverlappedElem, AActor* OtherActor);
};
