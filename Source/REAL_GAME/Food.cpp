// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "Snake.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>("Root");
	SetRootComponent(SceneComponent);
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	StaticMesh->SetupAttachment(SceneComponent);
	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &AFood::FoodBeginOverlap);
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	if(Big)
	{
		StaticMesh->SetRelativeScale3D(StaticMesh->GetRelativeScale3D()*2);
	}
}

void AFood::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Smash(AActor* Object, bool IsThisHead)
{
	if(IsThisHead)
	{
		auto Snake = Cast<ASnake>(Object);
		if(IsValid(Snake))
		{
			Snake->AddElements(Big ? 3 : 1);
			Eaten = true;
			this->Destroy();
		}
	}
}

void AFood::FoodBeginOverlap(UPrimitiveComponent* OverlapedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	Overlapped = true;
}

