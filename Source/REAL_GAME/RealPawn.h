// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "RealPawn.generated.h"

class UCameraComponent;
class ASnake;
class UInputMappingContext;
class UInputAction;
struct FInputActionValue;

UCLASS()
class REAL_GAME_API ARealPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ARealPawn();

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	UCameraComponent* Camera;

	UPROPERTY(BlueprintReadWrite)
	ASnake* SnakeBase;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnake> SnakeClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void SpawnSnake();

protected:
	UPROPERTY(EditAnywhere, Category = "EnhancedInput")
	UInputMappingContext* InputMapping;

	UPROPERTY(EditAnywhere, Category = "EnhancedInput")
	UInputAction* MoveAction;

	void Move(const FInputActionValue& Value);
};
