// Fill out your copyright notice in the Description page of Project Settings.

#include "RealPawn.h"
#include "Camera/CameraComponent.h"
#include "Snake.h"
#include "EnhancedInputSubsystems.h"
#include "InputMappingContext.h"
#include "EnhancedInputComponent.h"
#include "InputActionValue.h"
#include "SnakeElem.h"

// Sets default values
ARealPawn::ARealPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	RootComponent = Camera;
}

// Called when the game starts or when spawned
void ARealPawn::BeginPlay()
{
	Super::BeginPlay();
	
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(InputMapping, 0);
		}
	}

	SetActorRotation(FRotator(-90, 0, 0));
	SpawnSnake();
}

// Called every frame
void ARealPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ARealPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ARealPawn::Move);
	}
}

void ARealPawn::SpawnSnake()
{
	SnakeBase = GetWorld()->SpawnActor<ASnake>(SnakeClass, FTransform(FVector(0, 0, 100)));
}

void ARealPawn::Move(const FInputActionValue& Value)
{
	if (IsValid(SnakeBase))
	{
		float XMove = Value.Get<FVector2D>().X;
		float YMove = Value.Get<FVector2D>().Y;
		ASnakeElem* FirstSnakeElem = SnakeBase->SnakeBody[0];
		ASnakeElem* SecondSnakeElem = SnakeBase->SnakeBody[1];
	
		if (XMove > 0 && truncf(FirstSnakeElem->GetActorLocation().X) != truncf(SecondSnakeElem->GetActorLocation().X))
		{
			SnakeBase->LastDirection = EMovementDirection::RIGHT;
		}
		else if (XMove < 0 && truncf(FirstSnakeElem->GetActorLocation().X) != truncf(SecondSnakeElem->GetActorLocation().X))
		{
			SnakeBase->LastDirection = EMovementDirection::LEFT;
		}
	
		if (YMove > 0 && truncf(FirstSnakeElem->GetActorLocation().Y) != truncf(SecondSnakeElem->GetActorLocation().Y))
		{
			SnakeBase->LastDirection = EMovementDirection::UP;
		}
		else if (YMove < 0 && truncf(FirstSnakeElem->GetActorLocation().Y) != truncf(SecondSnakeElem->GetActorLocation().Y))
		{
			SnakeBase->LastDirection = EMovementDirection::DOWN;
		}
	}
}

