// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElem.h"
#include "Components/StaticMeshComponent.h"
#include "Snake.h"
// Sets default values
ASnakeElem::ASnakeElem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(SceneComponent);
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	Mesh->SetupAttachment(SceneComponent);
	Mesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Mesh->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void ASnakeElem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASnakeElem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElem::SetHeadElem_Implementation()
{
	Mesh->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElem::BeginOverlap);
}

void ASnakeElem::Smash(AActor* Object, bool IsThisHead)
{
	if(IsThisHead)
	{
		auto Snake = Cast<ASnake>(Object);
		if(IsValid(Snake))
		{
			Snake->Destroy();
		}
	}
}

void ASnakeElem::ElemBreake()
{
	
}

void ASnakeElem::ToggleCollision()
{
	if(Mesh->GetCollisionEnabled() == ECollisionEnabled::QueryOnly)
	{
		Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
	else
	{
		Mesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
}

void ASnakeElem::BeginOverlap(UPrimitiveComponent* OverlapedComponent,
                              AActor* OtherActor,
                              UPrimitiveComponent* OtherComp,
                              int32 OtherBodyIndex,
                              bool bFromSweep,
                              const FHitResult& SweepResult)
{
if (IsValid(SnakeOwner))
	{
		SnakeOwner->OnElemOverlap(this, OtherActor);
	}
}
