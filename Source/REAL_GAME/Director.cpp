// Fill out your copyright notice in the Description page of Project Settings.


#include "Director.h"
#include "Food.h"
#include "TestCollision.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
ADirector::ADirector()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ADirector::BeginPlay()
{
	Super::BeginPlay();
	SpawnFood();
}

// Called every frame
void ADirector::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADirector::SpawnFood()
{
	for(int i = CurRound; i > 0; i--)
	{
		FTransform SpawnPoint = FTransform( FVector(FMath::FRandRange(-600.f, 790.f), FMath::FRandRange(-550.f, 870.f), 100));
		ATestCollision* NewTest = GetWorld()->SpawnActor<ATestCollision>(TestType,SpawnPoint);
		if(NewTest->Overlapped)
		{
			NewTest->Destroy();
			++i;
			continue;
		}
		NewTest->Destroy();
		AFood* NewContent = GetWorld()->SpawnActor<AFood>(FoodType,SpawnPoint);
		NewContent->OnDestroyed.AddDynamic(this, &ADirector::AddScore);
		++CurContent;
	}

	for(int i = 1; i > 0; i--)
	{
		FTransform SpawnPoint = FTransform( FVector(FMath::FRandRange(-600.f, 790.f), FMath::FRandRange(-550.f, 870.f), 100));
		ATestCollision* NewTest = GetWorld()->SpawnActor<ATestCollision>(TestType,SpawnPoint);
		if(NewTest->Overlapped)
		{
			NewTest->Destroy();
			++i;
			continue;
		}
		NewTest->Destroy();
		AFood* NewContent = GetWorld()->SpawnActorDeferred<AFood>(FoodType,SpawnPoint);
		NewContent->Big = true;
		UGameplayStatics::FinishSpawningActor(NewContent, SpawnPoint);
		NewContent->OnDestroyed.AddDynamic(this, &ADirector::AddScore);
		NewRound();
		++CurContent;
	}

}

void ADirector::AddScore(AActor* Actor)
{
		CurContent --;
		if(CurContent == 0)
		{
			CurRound++;
			SpawnFood();
		}
}

void ADirector::NewRound_Implementation()
{
	
}
