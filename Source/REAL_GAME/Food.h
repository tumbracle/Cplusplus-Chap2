// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "IInteractable.h"
#include "GameFramework/Actor.h"
#include "Food.generated.h"

class UStaticMeshComponent;
class USceneComponent;

UCLASS()
class REAL_GAME_API AFood : public AActor, public IIInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	USceneComponent* SceneComponent;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool Big;

	UPROPERTY()
	bool Overlapped = false;

	UPROPERTY(BlueprintReadOnly,VisibleDefaultsOnly)
	bool Eaten = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void OnConstruction(const FTransform& Transform) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	virtual void Smash(AActor* Object, bool IsThisHead) override;

	UFUNCTION()
	void FoodBeginOverlap(UPrimitiveComponent* OverlapedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);

};
