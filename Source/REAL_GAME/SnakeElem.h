// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "IInteractable.h"
#include "SnakeElem.generated.h"

class UStaticMeshComponent;
class ASnake;

UCLASS()
class REAL_GAME_API ASnakeElem : public AActor, public IIInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeElem();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Mesh;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	USceneComponent* SceneComponent;

	UPROPERTY()
	ASnake* SnakeOwner;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent)
	void SetHeadElem();
	void SetHeadElem_Implementation();

	virtual void Smash(AActor* Object, bool IsThisHead) override;

	UFUNCTION()
	void BeginOverlap(UPrimitiveComponent* OverlapedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);

	UFUNCTION()
	void ElemBreake();

	void ToggleCollision();
};
