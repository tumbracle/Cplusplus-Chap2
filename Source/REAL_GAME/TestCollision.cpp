// Fill out your copyright notice in the Description page of Project Settings.


#include "TestCollision.h"
#include "Components/SphereComponent.h"

// Sets default values
ATestCollision::ATestCollision()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Collision = CreateDefaultSubobject<USphereComponent>(TEXT("Collision"));
	SetRootComponent(Collision);
	Collision->OnComponentBeginOverlap.AddDynamic(this, &ATestCollision::TESTBeginOverlap);

}

// Called when the game starts or when spawned
void ATestCollision::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATestCollision::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATestCollision::TESTBeginOverlap(UPrimitiveComponent* OverlapedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Overlapped = true;
}

