// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Director.generated.h"

class ATestCollision;
class AFood;

UCLASS()
class REAL_GAME_API ADirector : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADirector();

	UPROPERTY()
	int32 CurRound = 1;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TSubclassOf<AFood> FoodType;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TSubclassOf<ATestCollision> TestType;

	UPROPERTY()
	int32 CurContent = 0;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void SpawnFood();

	UFUNCTION()
	void AddScore(AActor* Actor);

	UFUNCTION(BlueprintNativeEvent)
	void NewRound();
	void NewRound_Implementation();
};
