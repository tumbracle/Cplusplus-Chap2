// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake.h"
#include "SnakeElem.h"
#include "IInteractable.h"

// Sets default values
ASnake::ASnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElemPadding = 100.f;
	LastDirection = EMovementDirection::UP;
	speed = 0.5;
	ElemsQueue = 0;
}

// Called when the game starts or when spawned
void ASnake::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(speed);
	FirstElemAdd(5);
}

// Called every frame
void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnake::AddElements(int ElemNum)
{
	for(int i = 0; i < ElemNum; i++)
	{
		FTransform SpawnPoint = FTransform(SnakeBody[SnakeBody.Num() - 1]->GetActorLocation());
		ASnakeElem* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElem>(BodyClass, SpawnPoint);
		NewSnakeElem->SetActorHiddenInGame(true);
		int32 ElemIndex = SnakeBody.Add(NewSnakeElem);
		NewSnakeElem->SnakeOwner = this;
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetHeadElem();
		}
	}
}

void ASnake::FirstElemAdd(int ElemNum)
{
	for(int i = 0; i < ElemNum; i++)
	{
		FTransform SpawnPoint = FTransform(GetActorLocation() - FVector(SnakeBody.Num() * ElemPadding, 0 , 0));
		ASnakeElem* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElem>(BodyClass, SpawnPoint);
		NewSnakeElem->SetActorHiddenInGame(true);
		int32 ElemIndex = SnakeBody.Add(NewSnakeElem);
		NewSnakeElem->SnakeOwner = this;
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetHeadElem();
		}
	}
}

void ASnake::Move()
{
	FVector MovementDirection;

	switch (LastDirection)
	{
	case EMovementDirection::UP:
		MovementDirection.X += ElemPadding;
		break;
	case EMovementDirection::DOWN:
		MovementDirection.X -= ElemPadding;
		break;
	case EMovementDirection::RIGHT:
		MovementDirection.Y += ElemPadding;
		break;
	case EMovementDirection::LEFT:
		MovementDirection.Y -= ElemPadding;
		break;
	}

	SnakeBody[0]->ToggleCollision();
	
	for (int i = SnakeBody.Num() - 1; i > 0; --i)
	{
		auto CurElem = SnakeBody[i];
		auto PrevElem = SnakeBody[i - 1];
		FVector PrevLocation = PrevElem->GetActorLocation();
		CurElem->SetActorLocation(PrevLocation);
		SnakeBody[i]->SetActorHiddenInGame(false);
	}
	SnakeBody[0]->AddActorWorldOffset(MovementDirection);
	SnakeBody[0]->ToggleCollision();
	SnakeBody[0]->SetActorHiddenInGame(false);
}

void ASnake::OnElemOverlap(ASnakeElem* OverlappedElem, AActor* OtherActor)
{
	if(IsValid(OverlappedElem))
	{
		int32 ElemIndex;
		SnakeBody.Find(OverlappedElem, ElemIndex);
		bool IsHead = ElemIndex == 0;
		if(IIInteractable* InteractInterface = Cast<IIInteractable>(OtherActor))
		{
			InteractInterface->Smash(this, IsHead);
		}
	}
}

