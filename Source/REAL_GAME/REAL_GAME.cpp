// Copyright Epic Games, Inc. All Rights Reserved.

#include "REAL_GAME.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, REAL_GAME, "REAL_GAME" );
